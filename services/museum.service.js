'use strict';

// packages
const moment = require('moment')

// models
const visitorsModel = require('../models/visitors.model');

// function to get visitors statistics for the provided parameters
let getVisitorsStats = async (date, ignore) => {
    try {
        // get month and year from the date
        let monthYear = moment(Number(date)).format("MM:yyyy");
        let month = moment(Number(date)).format("MMM");
        let year = moment(Number(date)).format("yyyy");

        // to build dynamic query 
        let query = [];
        // to ignore a perticular museum and visitorCount with null values
        if (ignore) query.push({ $match: { museumName: { $ne: ignore }, visitorCount: { $ne: null } } }) 
        // add additional field monthYear to match month and year from any given date
        query.push({ $addFields: { "monthYear": { $dateToString: { format: "%m:%G", date: "$visitDate" } } } }) 
        query.push({ $match: { monthYear: monthYear } })
        query.push({ $sort: { visitorCount: -1 } })
        query.push({ $project: { _id: 0, museum: "$museumName", visitors: "$visitorCount" } })
        query.push({ $group: { _id: null, vGroup: { $push: "$$ROOT" }, vCount: { $sum: "$visitors" } } })

        let vData = await visitorsModel.aggregate(query);
        let vLength = vData[0].vGroup.length;

        // format data as per requirement 
        let attendance = {
            "month": month,
            "year": year,
            "highest": vData[0].vGroup[0],
            "lowest": vData[0].vGroup[vLength - 1],
            "total": vData[0].vCount
        }

        if (ignore) {
            // to get visitors of the ignored museum
            let ignoredMuseum = await visitorsModel.aggregate([{
                $addFields: { "monthYear": { $dateToString: { format: "%m:%G", date: "$visitDate" } } }
            }, {
                $match: { museumName: ignore, monthYear: monthYear }
            }, {
                $project: { _id: 0, museum: "$museumName", visitors: "$visitorCount" }
            }])
            attendance.ignore = ignoredMuseum[0];
        }

        return { attendance };
    } catch (error) {
        console.error("error", error);
        throw error;
    }
}

module.exports = { getVisitorsStats }