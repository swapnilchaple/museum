const expect = require('chai').expect;
const request = require('supertest');

const app = require('../app.js');
const conn = require('../db/index.js');

describe('GET /visitors', () => {
    before((done) => {
        conn.connectDb()
            .then(() => done())
            .catch((err) => done(err));
    })

    after((done) => {
        conn.disconnectDb()
            .then(() => done())
            .catch((err) => done(err));
    })

    // a simple test to just check if the api is giving a success
    it('OK, getting visitors', (done) => {
        request(app).get('/api/visitors?date=1404198000000&ignore=Avila Adobe')
            .then((res) => {
                const body = res.body;
                expect(body.status).to.equal('success');
                done();
            })
            .catch((err) => done(err));
    });
})