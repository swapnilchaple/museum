let config = {
    production: {
        // database
        database: 'mongodb://localhost:27017/museumdb',
        autoIndex: true,
        mongooseDebug: false,
        keepAlive: true,
        useUnifiedTopology: true,
    },
    staging: {
        database: 'mongodb://localhost:27017/museumdb',
        autoIndex: true,
        mongooseDebug: false,
        keepAlive: true,
        useUnifiedTopology: true,
    }
}

let env = process.env.NODE_ENV ? process.env.NODE_ENV : 'staging';
console.log(`******** Environment - ${env} ********`);

module.exports = {
    config: config[env]
}