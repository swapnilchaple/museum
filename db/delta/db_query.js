// use query to insert data into database;
db.getCollection('visitors').insert([
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("01/01/2014 12:00:00 AM"),
        "visitorCount": 6602
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("02/01/2014 12:00:00 AM"),
        "visitorCount": 5029
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("03/01/2014 12:00:00 AM"),
        "visitorCount": 8129
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("04/01/2014 12:00:00 AM"),
        "visitorCount": 2824
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("05/01/2014 12:00:00 AM"),
        "visitorCount": 10694
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("06/01/2014 12:00:00 AM"),
        "visitorCount": 11036
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("07/01/2014 12:00:00 AM"),
        "visitorCount": 13490
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("08/01/2014 12:00:00 AM"),
        "visitorCount": 9139
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("09/01/2014 12:00:00 AM"),
        "visitorCount": 5661
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("10/01/2014 12:00:00 AM"),
        "visitorCount": 7356
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("11/01/2014 12:00:00 AM"),
        "visitorCount": 9773
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("12/01/2014 12:00:00 AM"),
        "visitorCount": 7184
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("01/01/2015 12:00:00 AM"),
        "visitorCount": 6250
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("02/01/2015 12:00:00 AM"),
        "visitorCount": 5907
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("03/01/2015 12:00:00 AM"),
        "visitorCount": 9884
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("04/01/2015 12:00:00 AM"),
        "visitorCount": 7254
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("05/01/2015 12:00:00 AM"),
        "visitorCount": 13207
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("07/01/2015 12:00:00 AM"),
        "visitorCount": 11072
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("06/01/2015 12:00:00 AM"),
        "visitorCount": 11102
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("08/01/2015 12:00:00 AM"),
        "visitorCount": 12096
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("09/01/2015 12:00:00 AM"),
        "visitorCount": 6608
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("10/01/2015 12:00:00 AM"),
        "visitorCount": 12524
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("11/01/2015 12:00:00 AM"),
        "visitorCount": 6677
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("12/01/2015 12:00:00 AM"),
        "visitorCount": 5967
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("01/01/2016 12:00:00 AM"),
        "visitorCount": 6587
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("02/01/2016 12:00:00 AM"),
        "visitorCount": 6955
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("05/01/2017 12:00:00 AM"),
        "visitorCount": 7556
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("06/01/2017 12:00:00 AM"),
        "visitorCount": 6818
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("03/01/2016 12:00:00 AM"),
        "visitorCount": 9979
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("08/01/2016 12:00:00 AM"),
        "visitorCount": 6245
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("09/01/2016 12:00:00 AM"),
        "visitorCount": 6977
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("10/01/2016 12:00:00 AM"),
        "visitorCount": 6282
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("11/01/2016 12:00:00 AM"),
        "visitorCount": 6365
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("12/01/2016 12:00:00 AM"),
        "visitorCount": 5223
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("01/01/2017 12:00:00 AM"),
        "visitorCount": 4929
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("02/01/2017 12:00:00 AM"),
        "visitorCount": 4493
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("03/01/2017 12:00:00 AM"),
        "visitorCount": 6271
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("04/01/2017 12:00:00 AM"),
        "visitorCount": 6685
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("05/01/2016 12:00:00 AM"),
        "visitorCount": 7757
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("06/01/2016 12:00:00 AM"),
        "visitorCount": 6751
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("04/01/2016 12:00:00 AM"),
        "visitorCount": 5113
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("07/01/2016 12:00:00 AM"),
        "visitorCount": 12188
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("07/01/2017 12:00:00 AM"),
        "visitorCount": 8675
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("08/01/2017 12:00:00 AM"),
        "visitorCount": 6212
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("09/01/2017 12:00:00 AM"),
        "visitorCount": 4607
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("10/01/2017 12:00:00 AM"),
        "visitorCount": 6097
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("11/01/2017 12:00:00 AM"),
        "visitorCount": 6757
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("12/01/2017 12:00:00 AM"),
        "visitorCount": 6780
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("03/01/2018 12:00:00 AM"),
        "visitorCount": 4909
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("02/01/2018 12:00:00 AM"),
        "visitorCount": 5705
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("04/01/2018 12:00:00 AM"),
        "visitorCount": 4650
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("01/01/2018 12:00:00 AM"),
        "visitorCount": 5837
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("05/01/2018 12:00:00 AM"),
        "visitorCount": 5626
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("06/01/2018 12:00:00 AM"),
        "visitorCount": 4694
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("07/01/2018 12:00:00 AM"),
        "visitorCount": 4718
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("08/01/2018 12:00:00 AM"),
        "visitorCount": 3891
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("09/01/2018 12:00:00 AM"),
        "visitorCount": 3180
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("10/01/2018 12:00:00 AM"),
        "visitorCount": 3775
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("11/01/2018 12:00:00 AM"),
        "visitorCount": 4562
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("12/01/2018 12:00:00 AM"),
        "visitorCount": 4081
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("01/01/2019 12:00:00 AM"),
        "visitorCount": 3424
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("02/01/2019 12:00:00 AM"),
        "visitorCount": 2907
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("03/01/2019 12:00:00 AM"),
        "visitorCount": 4384
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("04/01/2019 12:00:00 AM"),
        "visitorCount": 4830
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("05/01/2019 12:00:00 AM"),
        "visitorCount": 6230
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("06/01/2019 12:00:00 AM"),
        "visitorCount": 3995
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("07/01/2019 12:00:00 AM"),
        "visitorCount": 4491
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("08/01/2019 12:00:00 AM"),
        "visitorCount": 3410
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("09/01/2019 12:00:00 AM"),
        "visitorCount": 3229
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("10/01/2019 12:00:00 AM"),
        "visitorCount": 4200
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("11/01/2019 12:00:00 AM"),
        "visitorCount": 4362
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("12/01/2019 12:00:00 AM"),
        "visitorCount": 2702
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("01/01/2020 12:00:00 AM"),
        "visitorCount": 3508
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("02/01/2020 12:00:00 AM"),
        "visitorCount": 3075
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("03/01/2020 12:00:00 AM"),
        "visitorCount": 1062
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("04/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("05/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("06/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("07/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("08/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("09/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("10/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("11/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("12/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("01/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("02/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("03/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("04/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("05/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("06/01/2021 12:00:00 AM"),
        "visitorCount": 1047
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("07/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("08/01/2021 12:00:00 AM"),
        "visitorCount": 892
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("09/01/2021 12:00:00 AM"),
        "visitorCount": 2225
    },
    {
        "museumName": "America Tropical Interpretive Center",
        "visitDate": new Date("10/01/2021 12:00:00 AM"),
        "visitorCount": 280
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("01/01/2014 12:00:00 AM"),
        "visitorCount": 24778
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("02/01/2014 12:00:00 AM"),
        "visitorCount": 18976
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("03/01/2014 12:00:00 AM"),
        "visitorCount": 25231
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("04/01/2014 12:00:00 AM"),
        "visitorCount": 26989
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("05/01/2014 12:00:00 AM"),
        "visitorCount": 36883
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("06/01/2014 12:00:00 AM"),
        "visitorCount": 29487
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("07/01/2014 12:00:00 AM"),
        "visitorCount": 32378
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("08/01/2014 12:00:00 AM"),
        "visitorCount": 37680
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("09/01/2014 12:00:00 AM"),
        "visitorCount": 28473
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("10/01/2014 12:00:00 AM"),
        "visitorCount": 27995
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("11/01/2014 12:00:00 AM"),
        "visitorCount": 25691
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("12/01/2014 12:00:00 AM"),
        "visitorCount": 18754
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("01/01/2015 12:00:00 AM"),
        "visitorCount": 20438
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("02/01/2015 12:00:00 AM"),
        "visitorCount": 15578
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("03/01/2015 12:00:00 AM"),
        "visitorCount": 21297
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("04/01/2015 12:00:00 AM"),
        "visitorCount": 26670
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("05/01/2015 12:00:00 AM"),
        "visitorCount": 34383
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("07/01/2015 12:00:00 AM"),
        "visitorCount": 41242
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("06/01/2015 12:00:00 AM"),
        "visitorCount": 30569
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("08/01/2015 12:00:00 AM"),
        "visitorCount": 30700
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("09/01/2015 12:00:00 AM"),
        "visitorCount": 20967
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("10/01/2015 12:00:00 AM"),
        "visitorCount": 29764
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("11/01/2015 12:00:00 AM"),
        "visitorCount": 24483
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("12/01/2015 12:00:00 AM"),
        "visitorCount": 21426
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("01/01/2016 12:00:00 AM"),
        "visitorCount": 19659
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("02/01/2016 12:00:00 AM"),
        "visitorCount": 17378
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("05/01/2017 12:00:00 AM"),
        "visitorCount": 30029
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("06/01/2017 12:00:00 AM"),
        "visitorCount": 22169
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("03/01/2016 12:00:00 AM"),
        "visitorCount": 20322
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("08/01/2016 12:00:00 AM"),
        "visitorCount": 25987
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("09/01/2016 12:00:00 AM"),
        "visitorCount": 22897
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("10/01/2016 12:00:00 AM"),
        "visitorCount": 25040
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("11/01/2016 12:00:00 AM"),
        "visitorCount": 17760
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("12/01/2016 12:00:00 AM"),
        "visitorCount": 20107
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("01/01/2017 12:00:00 AM"),
        "visitorCount": 18792
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("02/01/2017 12:00:00 AM"),
        "visitorCount": 14035
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("03/01/2017 12:00:00 AM"),
        "visitorCount": 20680
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("04/01/2017 12:00:00 AM"),
        "visitorCount": 25234
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("05/01/2016 12:00:00 AM"),
        "visitorCount": 31728
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("06/01/2016 12:00:00 AM"),
        "visitorCount": 23696
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("04/01/2016 12:00:00 AM"),
        "visitorCount": 24521
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("07/01/2016 12:00:00 AM"),
        "visitorCount": 31689
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("07/01/2017 12:00:00 AM"),
        "visitorCount": 30831
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("08/01/2017 12:00:00 AM"),
        "visitorCount": 27009
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("09/01/2017 12:00:00 AM"),
        "visitorCount": 23403
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("10/01/2017 12:00:00 AM"),
        "visitorCount": 22164
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("11/01/2017 12:00:00 AM"),
        "visitorCount": 17629
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("12/01/2017 12:00:00 AM"),
        "visitorCount": 18339
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("03/01/2018 12:00:00 AM"),
        "visitorCount": 19001
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("02/01/2018 12:00:00 AM"),
        "visitorCount": 14718
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("04/01/2018 12:00:00 AM"),
        "visitorCount": 18966
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("01/01/2018 12:00:00 AM"),
        "visitorCount": 16265
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("05/01/2018 12:00:00 AM"),
        "visitorCount": 25173
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("06/01/2018 12:00:00 AM"),
        "visitorCount": 22171
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("07/01/2018 12:00:00 AM"),
        "visitorCount": 23136
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("08/01/2018 12:00:00 AM"),
        "visitorCount": 20815
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("09/01/2018 12:00:00 AM"),
        "visitorCount": 21020
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("10/01/2018 12:00:00 AM"),
        "visitorCount": 19280
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("11/01/2018 12:00:00 AM"),
        "visitorCount": 17163
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("12/01/2018 12:00:00 AM"),
        "visitorCount": 17722
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("01/01/2019 12:00:00 AM"),
        "visitorCount": 13958
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("02/01/2019 12:00:00 AM"),
        "visitorCount": 10737
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("03/01/2019 12:00:00 AM"),
        "visitorCount": 17056
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("04/01/2019 12:00:00 AM"),
        "visitorCount": 21074
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("05/01/2019 12:00:00 AM"),
        "visitorCount": 26041
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("06/01/2019 12:00:00 AM"),
        "visitorCount": 20226
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("07/01/2019 12:00:00 AM"),
        "visitorCount": 24097
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("08/01/2019 12:00:00 AM"),
        "visitorCount": 20921
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("09/01/2019 12:00:00 AM"),
        "visitorCount": 17689
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("10/01/2019 12:00:00 AM"),
        "visitorCount": 18822
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("11/01/2019 12:00:00 AM"),
        "visitorCount": 15507
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("12/01/2019 12:00:00 AM"),
        "visitorCount": 15398
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("01/01/2020 12:00:00 AM"),
        "visitorCount": 15518
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("02/01/2020 12:00:00 AM"),
        "visitorCount": 12997
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("03/01/2020 12:00:00 AM"),
        "visitorCount": 4520
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("04/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("05/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("06/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("07/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("08/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("09/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("10/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("11/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("12/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("01/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("02/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("03/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("04/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("05/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("06/01/2021 12:00:00 AM"),
        "visitorCount": 3710
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("07/01/2021 12:00:00 AM"),
        "visitorCount": 7933
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("08/01/2021 12:00:00 AM"),
        "visitorCount": 6214
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("09/01/2021 12:00:00 AM"),
        "visitorCount": 5500
    },
    {
        "museumName": "Avila Adobe",
        "visitDate": new Date("10/01/2021 12:00:00 AM"),
        "visitorCount": 7201
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("01/01/2014 12:00:00 AM"),
        "visitorCount": 1581
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("02/01/2014 12:00:00 AM"),
        "visitorCount": 1785
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("03/01/2014 12:00:00 AM"),
        "visitorCount": 3229
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("04/01/2014 12:00:00 AM"),
        "visitorCount": 2129
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("05/01/2014 12:00:00 AM"),
        "visitorCount": 3676
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("06/01/2014 12:00:00 AM"),
        "visitorCount": 2121
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("07/01/2014 12:00:00 AM"),
        "visitorCount": 2239
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("08/01/2014 12:00:00 AM"),
        "visitorCount": 1769
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("09/01/2014 12:00:00 AM"),
        "visitorCount": 1073
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("10/01/2014 12:00:00 AM"),
        "visitorCount": 1979
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("11/01/2014 12:00:00 AM"),
        "visitorCount": 2404
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("12/01/2014 12:00:00 AM"),
        "visitorCount": 1319
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("01/01/2015 12:00:00 AM"),
        "visitorCount": 1823
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("02/01/2015 12:00:00 AM"),
        "visitorCount": 1558
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("03/01/2015 12:00:00 AM"),
        "visitorCount": 2336
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("04/01/2015 12:00:00 AM"),
        "visitorCount": 3057
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("05/01/2015 12:00:00 AM"),
        "visitorCount": 4009
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("07/01/2015 12:00:00 AM"),
        "visitorCount": 3057
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("06/01/2015 12:00:00 AM"),
        "visitorCount": 2544
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("08/01/2015 12:00:00 AM"),
        "visitorCount": 2415
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("09/01/2015 12:00:00 AM"),
        "visitorCount": 1398
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("10/01/2015 12:00:00 AM"),
        "visitorCount": 2237
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("11/01/2015 12:00:00 AM"),
        "visitorCount": 2850
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("12/01/2015 12:00:00 AM"),
        "visitorCount": 2075
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("01/01/2016 12:00:00 AM"),
        "visitorCount": 2150
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("02/01/2016 12:00:00 AM"),
        "visitorCount": 2547
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("05/01/2017 12:00:00 AM"),
        "visitorCount": 5585
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("06/01/2017 12:00:00 AM"),
        "visitorCount": 2946
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("03/01/2016 12:00:00 AM"),
        "visitorCount": 7702
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("08/01/2016 12:00:00 AM"),
        "visitorCount": 2153
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("09/01/2016 12:00:00 AM"),
        "visitorCount": 1754
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("10/01/2016 12:00:00 AM"),
        "visitorCount": 2607
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("11/01/2016 12:00:00 AM"),
        "visitorCount": 2248
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("12/01/2016 12:00:00 AM"),
        "visitorCount": 2263
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("01/01/2017 12:00:00 AM"),
        "visitorCount": 2531
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("02/01/2017 12:00:00 AM"),
        "visitorCount": 2139
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("03/01/2017 12:00:00 AM"),
        "visitorCount": 5966
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("04/01/2017 12:00:00 AM"),
        "visitorCount": 3273
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("05/01/2016 12:00:00 AM"),
        "visitorCount": 4565
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("06/01/2016 12:00:00 AM"),
        "visitorCount": 3034
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("04/01/2016 12:00:00 AM"),
        "visitorCount": 3485
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("07/01/2016 12:00:00 AM"),
        "visitorCount": 2776
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("07/01/2017 12:00:00 AM"),
        "visitorCount": 2939
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("08/01/2017 12:00:00 AM"),
        "visitorCount": 1784
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("09/01/2017 12:00:00 AM"),
        "visitorCount": 1704
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("10/01/2017 12:00:00 AM"),
        "visitorCount": 2419
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("11/01/2017 12:00:00 AM"),
        "visitorCount": 2685
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("12/01/2017 12:00:00 AM"),
        "visitorCount": 2768
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("03/01/2018 12:00:00 AM"),
        "visitorCount": 4529
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("02/01/2018 12:00:00 AM"),
        "visitorCount": 2710
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("04/01/2018 12:00:00 AM"),
        "visitorCount": 2750
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("01/01/2018 12:00:00 AM"),
        "visitorCount": 2477
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("05/01/2018 12:00:00 AM"),
        "visitorCount": 4960
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("06/01/2018 12:00:00 AM"),
        "visitorCount": 2518
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("07/01/2018 12:00:00 AM"),
        "visitorCount": 2620
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("08/01/2018 12:00:00 AM"),
        "visitorCount": 2409
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("09/01/2018 12:00:00 AM"),
        "visitorCount": 2146
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("10/01/2018 12:00:00 AM"),
        "visitorCount": 2364
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("11/01/2018 12:00:00 AM"),
        "visitorCount": 2385
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("12/01/2018 12:00:00 AM"),
        "visitorCount": 2078
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("01/01/2019 12:00:00 AM"),
        "visitorCount": 2330
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("02/01/2019 12:00:00 AM"),
        "visitorCount": 2371
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("03/01/2019 12:00:00 AM"),
        "visitorCount": 3804
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("04/01/2019 12:00:00 AM"),
        "visitorCount": 3484
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("05/01/2019 12:00:00 AM"),
        "visitorCount": 4570
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("06/01/2019 12:00:00 AM"),
        "visitorCount": 2853
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("07/01/2019 12:00:00 AM"),
        "visitorCount": 2271
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("08/01/2019 12:00:00 AM"),
        "visitorCount": 2017
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("09/01/2019 12:00:00 AM"),
        "visitorCount": 1458
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("10/01/2019 12:00:00 AM"),
        "visitorCount": 1987
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("11/01/2019 12:00:00 AM"),
        "visitorCount": 2808
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("12/01/2019 12:00:00 AM"),
        "visitorCount": 2388
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("01/01/2020 12:00:00 AM"),
        "visitorCount": 2603
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("02/01/2020 12:00:00 AM"),
        "visitorCount": 2425
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("03/01/2020 12:00:00 AM"),
        "visitorCount": 524
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("04/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("05/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("06/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("07/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("08/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("09/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("10/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("11/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("12/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("01/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("02/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("03/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("04/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("05/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("06/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("07/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("08/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("09/01/2021 12:00:00 AM"),
        "visitorCount": 193
    },
    {
        "museumName": "Chinese American Museum",
        "visitDate": new Date("10/01/2021 12:00:00 AM"),
        "visitorCount": 502
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("01/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("02/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("03/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("04/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("05/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("06/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("07/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("08/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("09/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("10/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("11/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("12/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("01/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("02/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("03/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("04/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("05/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("07/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("06/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("08/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("09/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("10/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("11/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("12/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("01/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("02/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("05/01/2017 12:00:00 AM"),
        "visitorCount": 2581
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("06/01/2017 12:00:00 AM"),
        "visitorCount": 3248
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("03/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("08/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("09/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("10/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("11/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("12/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("01/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("02/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("03/01/2017 12:00:00 AM"),
        "visitorCount": 3116
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("04/01/2017 12:00:00 AM"),
        "visitorCount": 3440
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("05/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("06/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("04/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("07/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("07/01/2017 12:00:00 AM"),
        "visitorCount": 4227
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("08/01/2017 12:00:00 AM"),
        "visitorCount": 2650
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("09/01/2017 12:00:00 AM"),
        "visitorCount": 1825
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("10/01/2017 12:00:00 AM"),
        "visitorCount": 1250
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("11/01/2017 12:00:00 AM"),
        "visitorCount": 1476
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("12/01/2017 12:00:00 AM"),
        "visitorCount": 1316
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("03/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("02/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("04/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("01/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("05/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("06/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("07/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("08/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("09/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("10/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("11/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("12/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("01/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("02/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("03/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("04/01/2019 12:00:00 AM"),
        "visitorCount": 2055
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("05/01/2019 12:00:00 AM"),
        "visitorCount": 2699
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("06/01/2019 12:00:00 AM"),
        "visitorCount": 1065
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("07/01/2019 12:00:00 AM"),
        "visitorCount": 999
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("08/01/2019 12:00:00 AM"),
        "visitorCount": 977
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("09/01/2019 12:00:00 AM"),
        "visitorCount": 1338
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("10/01/2019 12:00:00 AM"),
        "visitorCount": 1269
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("11/01/2019 12:00:00 AM"),
        "visitorCount": 1566
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("12/01/2019 12:00:00 AM"),
        "visitorCount": 1211
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("01/01/2020 12:00:00 AM"),
        "visitorCount": 695
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("02/01/2020 12:00:00 AM"),
        "visitorCount": 652
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("03/01/2020 12:00:00 AM"),
        "visitorCount": 261
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("04/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("05/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("06/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("07/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("08/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("09/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("10/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("11/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("12/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("01/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("02/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("03/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("04/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("05/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("06/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("07/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("08/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("09/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Gateway to Nature Center",
        "visitDate": new Date("10/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("01/01/2014 12:00:00 AM"),
        "visitorCount": 4486
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("02/01/2014 12:00:00 AM"),
        "visitorCount": 4172
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("03/01/2014 12:00:00 AM"),
        "visitorCount": 7082
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("04/01/2014 12:00:00 AM"),
        "visitorCount": 6756
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("05/01/2014 12:00:00 AM"),
        "visitorCount": 10858
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("06/01/2014 12:00:00 AM"),
        "visitorCount": 5751
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("07/01/2014 12:00:00 AM"),
        "visitorCount": 5406
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("08/01/2014 12:00:00 AM"),
        "visitorCount": 8619
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("09/01/2014 12:00:00 AM"),
        "visitorCount": 61192
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("10/01/2014 12:00:00 AM"),
        "visitorCount": 6488
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("11/01/2014 12:00:00 AM"),
        "visitorCount": 4189
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("12/01/2014 12:00:00 AM"),
        "visitorCount": 4339
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("01/01/2015 12:00:00 AM"),
        "visitorCount": 3858
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("02/01/2015 12:00:00 AM"),
        "visitorCount": 3742
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("03/01/2015 12:00:00 AM"),
        "visitorCount": 5390
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("04/01/2015 12:00:00 AM"),
        "visitorCount": 7000
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("05/01/2015 12:00:00 AM"),
        "visitorCount": 12528
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("07/01/2015 12:00:00 AM"),
        "visitorCount": 6111
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("06/01/2015 12:00:00 AM"),
        "visitorCount": 5377
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("08/01/2015 12:00:00 AM"),
        "visitorCount": 5383
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("09/01/2015 12:00:00 AM"),
        "visitorCount": 5746
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("10/01/2015 12:00:00 AM"),
        "visitorCount": 8882
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("11/01/2015 12:00:00 AM"),
        "visitorCount": 6848
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("12/01/2015 12:00:00 AM"),
        "visitorCount": 4502
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("01/01/2016 12:00:00 AM"),
        "visitorCount": 4377
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("02/01/2016 12:00:00 AM"),
        "visitorCount": 3675
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("05/01/2017 12:00:00 AM"),
        "visitorCount": 8733
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("06/01/2017 12:00:00 AM"),
        "visitorCount": 4862
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("03/01/2016 12:00:00 AM"),
        "visitorCount": 6334
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("08/01/2016 12:00:00 AM"),
        "visitorCount": 5181
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("09/01/2016 12:00:00 AM"),
        "visitorCount": 4736
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("10/01/2016 12:00:00 AM"),
        "visitorCount": 6145
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("11/01/2016 12:00:00 AM"),
        "visitorCount": 3930
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("12/01/2016 12:00:00 AM"),
        "visitorCount": 3911
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("01/01/2017 12:00:00 AM"),
        "visitorCount": 3551
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("02/01/2017 12:00:00 AM"),
        "visitorCount": 3491
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("03/01/2017 12:00:00 AM"),
        "visitorCount": 5273
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("04/01/2017 12:00:00 AM"),
        "visitorCount": 5320
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("05/01/2016 12:00:00 AM"),
        "visitorCount": 9315
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("06/01/2016 12:00:00 AM"),
        "visitorCount": 4918
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("04/01/2016 12:00:00 AM"),
        "visitorCount": 6509
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("07/01/2016 12:00:00 AM"),
        "visitorCount": 5602
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("07/01/2017 12:00:00 AM"),
        "visitorCount": 5242
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("08/01/2017 12:00:00 AM"),
        "visitorCount": 5379
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("09/01/2017 12:00:00 AM"),
        "visitorCount": 4484
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("10/01/2017 12:00:00 AM"),
        "visitorCount": 4822
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("11/01/2017 12:00:00 AM"),
        "visitorCount": 4613
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("12/01/2017 12:00:00 AM"),
        "visitorCount": 4448
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("03/01/2018 12:00:00 AM"),
        "visitorCount": 5225
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("02/01/2018 12:00:00 AM"),
        "visitorCount": 3306
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("04/01/2018 12:00:00 AM"),
        "visitorCount": 4535
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("01/01/2018 12:00:00 AM"),
        "visitorCount": 3910
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("05/01/2018 12:00:00 AM"),
        "visitorCount": 8013
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("06/01/2018 12:00:00 AM"),
        "visitorCount": 4635
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("07/01/2018 12:00:00 AM"),
        "visitorCount": 4191
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("08/01/2018 12:00:00 AM"),
        "visitorCount": 4866
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("09/01/2018 12:00:00 AM"),
        "visitorCount": 4956
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("10/01/2018 12:00:00 AM"),
        "visitorCount": 4622
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("11/01/2018 12:00:00 AM"),
        "visitorCount": 4082
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("12/01/2018 12:00:00 AM"),
        "visitorCount": 4253
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("01/01/2019 12:00:00 AM"),
        "visitorCount": 3608
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("02/01/2019 12:00:00 AM"),
        "visitorCount": 2900
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("03/01/2019 12:00:00 AM"),
        "visitorCount": 5045
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("04/01/2019 12:00:00 AM"),
        "visitorCount": 5204
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("05/01/2019 12:00:00 AM"),
        "visitorCount": 8202
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("06/01/2019 12:00:00 AM"),
        "visitorCount": 4787
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("07/01/2019 12:00:00 AM"),
        "visitorCount": 4432
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("08/01/2019 12:00:00 AM"),
        "visitorCount": 4460
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("09/01/2019 12:00:00 AM"),
        "visitorCount": 4293
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("10/01/2019 12:00:00 AM"),
        "visitorCount": 4035
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("11/01/2019 12:00:00 AM"),
        "visitorCount": 4385
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("12/01/2019 12:00:00 AM"),
        "visitorCount": 3566
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("01/01/2020 12:00:00 AM"),
        "visitorCount": 3403
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("02/01/2020 12:00:00 AM"),
        "visitorCount": 3854
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("03/01/2020 12:00:00 AM"),
        "visitorCount": 1052
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("04/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("05/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("06/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("07/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("08/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("09/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("10/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("11/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("12/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("01/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("02/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("03/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("04/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("05/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("06/01/2021 12:00:00 AM"),
        "visitorCount": 1159
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("07/01/2021 12:00:00 AM"),
        "visitorCount": 2083
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("08/01/2021 12:00:00 AM"),
        "visitorCount": 1491
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("09/01/2021 12:00:00 AM"),
        "visitorCount": 1370
    },
    {
        "museumName": "Firehouse Museum",
        "visitDate": new Date("10/01/2021 12:00:00 AM"),
        "visitorCount": 1762
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("01/01/2014 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("02/01/2014 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("03/01/2014 12:00:00 AM"),
        "visitorCount": 70
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("04/01/2014 12:00:00 AM"),
        "visitorCount": 250
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("05/01/2014 12:00:00 AM"),
        "visitorCount": 135
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("06/01/2014 12:00:00 AM"),
        "visitorCount": 255
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("07/01/2014 12:00:00 AM"),
        "visitorCount": 120
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("08/01/2014 12:00:00 AM"),
        "visitorCount": 250
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("09/01/2014 12:00:00 AM"),
        "visitorCount": 1145
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("10/01/2014 12:00:00 AM"),
        "visitorCount": 550
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("11/01/2014 12:00:00 AM"),
        "visitorCount": 410
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("12/01/2014 12:00:00 AM"),
        "visitorCount": 565
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("01/01/2015 12:00:00 AM"),
        "visitorCount": 75
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("02/01/2015 12:00:00 AM"),
        "visitorCount": 160
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("03/01/2015 12:00:00 AM"),
        "visitorCount": 325
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("04/01/2015 12:00:00 AM"),
        "visitorCount": 2000
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("05/01/2015 12:00:00 AM"),
        "visitorCount": 475
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("07/01/2015 12:00:00 AM"),
        "visitorCount": 100
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("06/01/2015 12:00:00 AM"),
        "visitorCount": 200
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("08/01/2015 12:00:00 AM"),
        "visitorCount": 50
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("09/01/2015 12:00:00 AM"),
        "visitorCount": 125
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("10/01/2015 12:00:00 AM"),
        "visitorCount": 750
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("11/01/2015 12:00:00 AM"),
        "visitorCount": 950
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("12/01/2015 12:00:00 AM"),
        "visitorCount": 120
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("01/01/2016 12:00:00 AM"),
        "visitorCount": 50
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("02/01/2016 12:00:00 AM"),
        "visitorCount": 60
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("05/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("06/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("03/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("08/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("09/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("10/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("11/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("12/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("01/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("02/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("03/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("04/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("05/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("06/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("04/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("07/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("07/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("08/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("09/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("10/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("11/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("12/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("03/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("02/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("04/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("01/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("05/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("06/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("07/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("08/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("09/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("10/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("11/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("12/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("01/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("02/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("03/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("04/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("05/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("06/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("07/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("08/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("09/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("10/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("11/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("12/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("01/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("02/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("03/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("04/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("05/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("06/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("07/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("08/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("09/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("10/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("11/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("12/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("01/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("02/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("03/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("04/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("05/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("06/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("07/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("08/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("09/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Hellman Quon",
        "visitDate": new Date("10/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("01/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("02/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("03/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("04/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("05/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("06/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("07/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("08/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("09/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("10/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("11/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("12/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("01/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("02/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("03/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("04/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("05/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("07/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("06/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("08/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("09/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("10/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("11/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("12/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("01/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("02/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("05/01/2017 12:00:00 AM"),
        "visitorCount": 863
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("06/01/2017 12:00:00 AM"),
        "visitorCount": 872
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("03/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("08/01/2016 12:00:00 AM"),
        "visitorCount": 930
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("09/01/2016 12:00:00 AM"),
        "visitorCount": 857
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("10/01/2016 12:00:00 AM"),
        "visitorCount": 1009
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("11/01/2016 12:00:00 AM"),
        "visitorCount": 863
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("12/01/2016 12:00:00 AM"),
        "visitorCount": 772
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("01/01/2017 12:00:00 AM"),
        "visitorCount": 556
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("02/01/2017 12:00:00 AM"),
        "visitorCount": 619
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("03/01/2017 12:00:00 AM"),
        "visitorCount": 736
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("04/01/2017 12:00:00 AM"),
        "visitorCount": 668
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("05/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("06/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("04/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("07/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("07/01/2017 12:00:00 AM"),
        "visitorCount": 1431
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("08/01/2017 12:00:00 AM"),
        "visitorCount": 1194
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("09/01/2017 12:00:00 AM"),
        "visitorCount": 758
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("10/01/2017 12:00:00 AM"),
        "visitorCount": 1351
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("11/01/2017 12:00:00 AM"),
        "visitorCount": 1088
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("12/01/2017 12:00:00 AM"),
        "visitorCount": 1205
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("03/01/2018 12:00:00 AM"),
        "visitorCount": 965
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("02/01/2018 12:00:00 AM"),
        "visitorCount": 1562
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("04/01/2018 12:00:00 AM"),
        "visitorCount": 1109
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("01/01/2018 12:00:00 AM"),
        "visitorCount": 1069
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("05/01/2018 12:00:00 AM"),
        "visitorCount": 1601
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("06/01/2018 12:00:00 AM"),
        "visitorCount": 1232
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("07/01/2018 12:00:00 AM"),
        "visitorCount": 1323
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("08/01/2018 12:00:00 AM"),
        "visitorCount": 1147
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("09/01/2018 12:00:00 AM"),
        "visitorCount": 1031
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("10/01/2018 12:00:00 AM"),
        "visitorCount": 1159
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("11/01/2018 12:00:00 AM"),
        "visitorCount": 1078
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("12/01/2018 12:00:00 AM"),
        "visitorCount": 1168
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("01/01/2019 12:00:00 AM"),
        "visitorCount": 837
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("02/01/2019 12:00:00 AM"),
        "visitorCount": 816
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("03/01/2019 12:00:00 AM"),
        "visitorCount": 1315
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("04/01/2019 12:00:00 AM"),
        "visitorCount": 1336
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("05/01/2019 12:00:00 AM"),
        "visitorCount": 1883
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("06/01/2019 12:00:00 AM"),
        "visitorCount": 1177
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("07/01/2019 12:00:00 AM"),
        "visitorCount": 1480
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("08/01/2019 12:00:00 AM"),
        "visitorCount": 1258
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("09/01/2019 12:00:00 AM"),
        "visitorCount": 904
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("10/01/2019 12:00:00 AM"),
        "visitorCount": 1181
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("11/01/2019 12:00:00 AM"),
        "visitorCount": 1472
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("12/01/2019 12:00:00 AM"),
        "visitorCount": 1088
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("01/01/2020 12:00:00 AM"),
        "visitorCount": 1357
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("02/01/2020 12:00:00 AM"),
        "visitorCount": 1081
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("03/01/2020 12:00:00 AM"),
        "visitorCount": 260
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("04/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("05/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("06/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("07/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("08/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("09/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("10/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("11/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("12/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("01/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("02/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("03/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("04/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("05/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("06/01/2021 12:00:00 AM"),
        "visitorCount": 227
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("07/01/2021 12:00:00 AM"),
        "visitorCount": 602
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("08/01/2021 12:00:00 AM"),
        "visitorCount": 416
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("09/01/2021 12:00:00 AM"),
        "visitorCount": 482
    },
    {
        "museumName": "IAMLA",
        "visitDate": new Date("10/01/2021 12:00:00 AM"),
        "visitorCount": 515
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("01/01/2014 12:00:00 AM"),
        "visitorCount": 2204
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("02/01/2014 12:00:00 AM"),
        "visitorCount": 1330
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("03/01/2014 12:00:00 AM"),
        "visitorCount": 4320
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("04/01/2014 12:00:00 AM"),
        "visitorCount": 3277
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("05/01/2014 12:00:00 AM"),
        "visitorCount": 4122
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("06/01/2014 12:00:00 AM"),
        "visitorCount": 355
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("07/01/2014 12:00:00 AM"),
        "visitorCount": 3375
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("08/01/2014 12:00:00 AM"),
        "visitorCount": 1550
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("09/01/2014 12:00:00 AM"),
        "visitorCount": 1335
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("10/01/2014 12:00:00 AM"),
        "visitorCount": 1518
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("11/01/2014 12:00:00 AM"),
        "visitorCount": 7769
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("12/01/2014 12:00:00 AM"),
        "visitorCount": 1140
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("01/01/2015 12:00:00 AM"),
        "visitorCount": 200
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("02/01/2015 12:00:00 AM"),
        "visitorCount": 1075
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("03/01/2015 12:00:00 AM"),
        "visitorCount": 3445
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("04/01/2015 12:00:00 AM"),
        "visitorCount": 3292
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("05/01/2015 12:00:00 AM"),
        "visitorCount": 5042
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("07/01/2015 12:00:00 AM"),
        "visitorCount": 4225
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("06/01/2015 12:00:00 AM"),
        "visitorCount": 4552
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("08/01/2015 12:00:00 AM"),
        "visitorCount": 5974
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("09/01/2015 12:00:00 AM"),
        "visitorCount": 700
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("10/01/2015 12:00:00 AM"),
        "visitorCount": 4158
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("11/01/2015 12:00:00 AM"),
        "visitorCount": 9312
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("12/01/2015 12:00:00 AM"),
        "visitorCount": 170
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("01/01/2016 12:00:00 AM"),
        "visitorCount": 250
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("02/01/2016 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("05/01/2017 12:00:00 AM"),
        "visitorCount": 500
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("06/01/2017 12:00:00 AM"),
        "visitorCount": 40
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("03/01/2016 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("08/01/2016 12:00:00 AM"),
        "visitorCount": 475
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("09/01/2016 12:00:00 AM"),
        "visitorCount": 325
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("10/01/2016 12:00:00 AM"),
        "visitorCount": 1210
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("11/01/2016 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("12/01/2016 12:00:00 AM"),
        "visitorCount": 715
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("01/01/2017 12:00:00 AM"),
        "visitorCount": 610
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("02/01/2017 12:00:00 AM"),
        "visitorCount": 895
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("03/01/2017 12:00:00 AM"),
        "visitorCount": 625
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("04/01/2017 12:00:00 AM"),
        "visitorCount": 425
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("05/01/2016 12:00:00 AM"),
        "visitorCount": 265
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("06/01/2016 12:00:00 AM"),
        "visitorCount": 220
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("04/01/2016 12:00:00 AM"),
        "visitorCount": 50
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("07/01/2016 12:00:00 AM"),
        "visitorCount": 215
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("07/01/2017 12:00:00 AM"),
        "visitorCount": 295
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("08/01/2017 12:00:00 AM"),
        "visitorCount": 635
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("09/01/2017 12:00:00 AM"),
        "visitorCount": 920
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("10/01/2017 12:00:00 AM"),
        "visitorCount": 3580
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("11/01/2017 12:00:00 AM"),
        "visitorCount": 415
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("12/01/2017 12:00:00 AM"),
        "visitorCount": 605
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("03/01/2018 12:00:00 AM"),
        "visitorCount": 1645
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("02/01/2018 12:00:00 AM"),
        "visitorCount": 900
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("04/01/2018 12:00:00 AM"),
        "visitorCount": 1155
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("01/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("05/01/2018 12:00:00 AM"),
        "visitorCount": 250
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("06/01/2018 12:00:00 AM"),
        "visitorCount": 410
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("07/01/2018 12:00:00 AM"),
        "visitorCount": 327
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("08/01/2018 12:00:00 AM"),
        "visitorCount": 1119
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("09/01/2018 12:00:00 AM"),
        "visitorCount": 2140
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("10/01/2018 12:00:00 AM"),
        "visitorCount": 1975
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("11/01/2018 12:00:00 AM"),
        "visitorCount": 1680
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("12/01/2018 12:00:00 AM"),
        "visitorCount": 185
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("01/01/2019 12:00:00 AM"),
        "visitorCount": 4225
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("02/01/2019 12:00:00 AM"),
        "visitorCount": 475
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("03/01/2019 12:00:00 AM"),
        "visitorCount": 925
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("04/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("05/01/2019 12:00:00 AM"),
        "visitorCount": 290
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("06/01/2019 12:00:00 AM"),
        "visitorCount": 1800
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("07/01/2019 12:00:00 AM"),
        "visitorCount": 90
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("08/01/2019 12:00:00 AM"),
        "visitorCount": 120
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("09/01/2019 12:00:00 AM"),
        "visitorCount": 740
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("10/01/2019 12:00:00 AM"),
        "visitorCount": 1080
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("11/01/2019 12:00:00 AM"),
        "visitorCount": 225
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("12/01/2019 12:00:00 AM"),
        "visitorCount": 485
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("01/01/2020 12:00:00 AM"),
        "visitorCount": 125
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("02/01/2020 12:00:00 AM"),
        "visitorCount": 150
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("03/01/2020 12:00:00 AM"),
        "visitorCount": 140
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("04/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("05/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("06/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("07/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("08/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("09/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("10/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("11/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("12/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("01/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("02/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("03/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("04/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("05/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("06/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("07/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("08/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("09/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Pico House",
        "visitDate": new Date("10/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("01/01/2014 12:00:00 AM"),
        "visitorCount": 2961
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("02/01/2014 12:00:00 AM"),
        "visitorCount": 2276
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("03/01/2014 12:00:00 AM"),
        "visitorCount": 3116
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("04/01/2014 12:00:00 AM"),
        "visitorCount": 2808
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("05/01/2014 12:00:00 AM"),
        "visitorCount": 3987
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("06/01/2014 12:00:00 AM"),
        "visitorCount": 3133
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("07/01/2014 12:00:00 AM"),
        "visitorCount": 3527
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("08/01/2014 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("09/01/2014 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("10/01/2014 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("11/01/2014 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("12/01/2014 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("01/01/2015 12:00:00 AM"),
        "visitorCount": 2082
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("02/01/2015 12:00:00 AM"),
        "visitorCount": 2751
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("03/01/2015 12:00:00 AM"),
        "visitorCount": 2748
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("04/01/2015 12:00:00 AM"),
        "visitorCount": 3408
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("05/01/2015 12:00:00 AM"),
        "visitorCount": 3785
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("07/01/2015 12:00:00 AM"),
        "visitorCount": 4233
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("06/01/2015 12:00:00 AM"),
        "visitorCount": 3444
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("08/01/2015 12:00:00 AM"),
        "visitorCount": 3606
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("09/01/2015 12:00:00 AM"),
        "visitorCount": 3323
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("10/01/2015 12:00:00 AM"),
        "visitorCount": 3209
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("11/01/2015 12:00:00 AM"),
        "visitorCount": 13750
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("12/01/2015 12:00:00 AM"),
        "visitorCount": 3367
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("01/01/2016 12:00:00 AM"),
        "visitorCount": 2940
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("02/01/2016 12:00:00 AM"),
        "visitorCount": 2288
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("05/01/2017 12:00:00 AM"),
        "visitorCount": 2582
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("06/01/2017 12:00:00 AM"),
        "visitorCount": 2372
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("03/01/2016 12:00:00 AM"),
        "visitorCount": 3839
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("08/01/2016 12:00:00 AM"),
        "visitorCount": 2723
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("09/01/2016 12:00:00 AM"),
        "visitorCount": 1221
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("10/01/2016 12:00:00 AM"),
        "visitorCount": 1769
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("11/01/2016 12:00:00 AM"),
        "visitorCount": 1420
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("12/01/2016 12:00:00 AM"),
        "visitorCount": 973
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("01/01/2017 12:00:00 AM"),
        "visitorCount": 1325
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("02/01/2017 12:00:00 AM"),
        "visitorCount": 1049
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("03/01/2017 12:00:00 AM"),
        "visitorCount": 1366
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("04/01/2017 12:00:00 AM"),
        "visitorCount": 3070
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("05/01/2016 12:00:00 AM"),
        "visitorCount": 3408
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("06/01/2016 12:00:00 AM"),
        "visitorCount": 1876
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("04/01/2016 12:00:00 AM"),
        "visitorCount": 2797
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("07/01/2016 12:00:00 AM"),
        "visitorCount": 2951
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("07/01/2017 12:00:00 AM"),
        "visitorCount": 2771
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("08/01/2017 12:00:00 AM"),
        "visitorCount": 2182
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("09/01/2017 12:00:00 AM"),
        "visitorCount": 2130
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("10/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("11/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("12/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("03/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("02/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("04/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("01/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("05/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("06/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("07/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("08/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("09/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("10/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("11/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("12/01/2018 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("01/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("02/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("03/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("04/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("05/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("06/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("07/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("08/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("09/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("10/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("11/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("12/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("01/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("02/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("03/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("04/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("05/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("06/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("07/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("08/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("09/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("10/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("11/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("12/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("01/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("02/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("03/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("04/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("05/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("06/01/2021 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("07/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("08/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("09/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Visitor Center/ El Tranquilo Gallery",
        "visitDate": new Date("10/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("01/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("02/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("03/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("04/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("05/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("06/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("07/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("08/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("09/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("10/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("11/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("12/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("01/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("02/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("03/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("04/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("05/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("07/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("06/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("08/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("09/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("10/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("11/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("12/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("01/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("02/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("05/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("06/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("03/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("08/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("09/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("10/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("11/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("12/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("01/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("02/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("03/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("04/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("05/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("06/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("04/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("07/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("07/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("08/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("09/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("10/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("11/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("12/01/2017 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("03/01/2018 12:00:00 AM"),
        "visitorCount": 3348
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("02/01/2018 12:00:00 AM"),
        "visitorCount": 3621
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("04/01/2018 12:00:00 AM"),
        "visitorCount": 2956
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("01/01/2018 12:00:00 AM"),
        "visitorCount": 3585
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("05/01/2018 12:00:00 AM"),
        "visitorCount": 2332
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("06/01/2018 12:00:00 AM"),
        "visitorCount": 2351
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("07/01/2018 12:00:00 AM"),
        "visitorCount": 3686
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("08/01/2018 12:00:00 AM"),
        "visitorCount": 2604
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("09/01/2018 12:00:00 AM"),
        "visitorCount": 2670
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("10/01/2018 12:00:00 AM"),
        "visitorCount": 3055
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("11/01/2018 12:00:00 AM"),
        "visitorCount": 2919
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("12/01/2018 12:00:00 AM"),
        "visitorCount": 2694
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("01/01/2019 12:00:00 AM"),
        "visitorCount": 2670
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("02/01/2019 12:00:00 AM"),
        "visitorCount": 2080
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("03/01/2019 12:00:00 AM"),
        "visitorCount": 2819
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("04/01/2019 12:00:00 AM"),
        "visitorCount": 1933
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("05/01/2019 12:00:00 AM"),
        "visitorCount": 1959
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("06/01/2019 12:00:00 AM"),
        "visitorCount": 2557
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("07/01/2019 12:00:00 AM"),
        "visitorCount": 2911
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("08/01/2019 12:00:00 AM"),
        "visitorCount": 1701
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("09/01/2019 12:00:00 AM"),
        "visitorCount": 2722
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("10/01/2019 12:00:00 AM"),
        "visitorCount": 2878
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("11/01/2019 12:00:00 AM"),
        "visitorCount": 10740
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("12/01/2019 12:00:00 AM"),
        "visitorCount": 1948
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("01/01/2020 12:00:00 AM"),
        "visitorCount": 2958
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("02/01/2020 12:00:00 AM"),
        "visitorCount": 2591
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("03/01/2020 12:00:00 AM"),
        "visitorCount": 373
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("04/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("05/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("06/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("07/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("08/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("09/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("10/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("11/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("12/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("01/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("02/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("03/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("04/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("05/01/2021 12:00:00 AM"),
        "visitorCount": 4710
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("06/01/2021 12:00:00 AM"),
        "visitorCount": 3235
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("07/01/2021 12:00:00 AM"),
        "visitorCount": 1872
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("08/01/2021 12:00:00 AM"),
        "visitorCount": 1591
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("09/01/2021 12:00:00 AM"),
        "visitorCount": 817
    },
    {
        "museumName": "Museum of Social Justice",
        "visitDate": new Date("10/01/2021 12:00:00 AM"),
        "visitorCount": 1347
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("01/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("02/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("03/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("04/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("05/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("06/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("07/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("08/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("09/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("10/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("11/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("12/01/2014 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("01/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("02/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("03/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("04/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("05/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("07/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("06/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("08/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("09/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("10/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("11/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("12/01/2015 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("01/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("02/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("05/01/2017 12:00:00 AM"),
        "visitorCount": 930
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("06/01/2017 12:00:00 AM"),
        "visitorCount": 891
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("03/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("08/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("09/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("10/01/2016 12:00:00 AM"),
        "visitorCount": 760
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("11/01/2016 12:00:00 AM"),
        "visitorCount": 1162
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("12/01/2016 12:00:00 AM"),
        "visitorCount": 692
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("01/01/2017 12:00:00 AM"),
        "visitorCount": 518
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("02/01/2017 12:00:00 AM"),
        "visitorCount": 879
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("03/01/2017 12:00:00 AM"),
        "visitorCount": 814
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("04/01/2017 12:00:00 AM"),
        "visitorCount": 1124
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("05/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("06/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("04/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("07/01/2016 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("07/01/2017 12:00:00 AM"),
        "visitorCount": 1156
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("08/01/2017 12:00:00 AM"),
        "visitorCount": 862
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("09/01/2017 12:00:00 AM"),
        "visitorCount": 960
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("10/01/2017 12:00:00 AM"),
        "visitorCount": 536
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("11/01/2017 12:00:00 AM"),
        "visitorCount": 1586
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("12/01/2017 12:00:00 AM"),
        "visitorCount": 1292
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("03/01/2018 12:00:00 AM"),
        "visitorCount": 1320
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("02/01/2018 12:00:00 AM"),
        "visitorCount": 1148
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("04/01/2018 12:00:00 AM"),
        "visitorCount": 1790
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("01/01/2018 12:00:00 AM"),
        "visitorCount": 1141
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("05/01/2018 12:00:00 AM"),
        "visitorCount": 1628
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("06/01/2018 12:00:00 AM"),
        "visitorCount": 1434
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("07/01/2018 12:00:00 AM"),
        "visitorCount": 1422
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("08/01/2018 12:00:00 AM"),
        "visitorCount": 1331
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("09/01/2018 12:00:00 AM"),
        "visitorCount": 926
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("10/01/2018 12:00:00 AM"),
        "visitorCount": 1210
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("11/01/2018 12:00:00 AM"),
        "visitorCount": 822
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("12/01/2018 12:00:00 AM"),
        "visitorCount": 349
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("01/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("02/01/2019 12:00:00 AM"),
        "visitorCount": 406
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("03/01/2019 12:00:00 AM"),
        "visitorCount": 1038
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("04/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("05/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("06/01/2019 12:00:00 AM"),
        "visitorCount": 933
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("07/01/2019 12:00:00 AM"),
        "visitorCount": 774
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("08/01/2019 12:00:00 AM"),
        "visitorCount": 872
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("09/01/2019 12:00:00 AM"),
        "visitorCount": 569
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("10/01/2019 12:00:00 AM"),
        "visitorCount": 871
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("11/01/2019 12:00:00 AM"),
        "visitorCount": 1976
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("12/01/2019 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("01/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("02/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("03/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("04/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("05/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("06/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("07/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("08/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("09/01/2020 12:00:00 AM"),
        "visitorCount": null
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("10/01/2020 12:00:00 AM"),
        "visitorCount": 2533
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("11/01/2020 12:00:00 AM"),
        "visitorCount": 3801
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("12/01/2020 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("01/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("02/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("03/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("04/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("05/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("06/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("07/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("08/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("09/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
    {
        "museumName": "Biscailuz Gallery/ PK Outdoor Exhibit",
        "visitDate": new Date("10/01/2021 12:00:00 AM"),
        "visitorCount": 0
    },
])