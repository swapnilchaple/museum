const mongoose = require('mongoose');

// config
const config = require('../config/config').config;

const connectDb = () => {
    return new Promise((resolve, reject) => {
        mongoose.connect(config.database, {
            socketTimeoutMS: 50000,
            keepAlive: config.keepAlive,
            useUnifiedTopology: config.useUnifiedTopology,
            autoIndex: config.autoIndex // must be false in production
        }).then(function () {
            console.log(`connetion to database successful`);
            resolve();
        }, function (err) {
            console.log("database connetion error", err);
            reject(err);
        });
    })
}

const disconnectDb = () => {
    return mongoose.disconnect()
}

module.exports = { connectDb, disconnectDb }