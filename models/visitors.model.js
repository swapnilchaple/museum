const mongoose = require('mongoose');

const visitorSchema = mongoose.Schema({
    museumName: String,
    visitDate: Date,
    visitorCount: Number
})

module.exports = mongoose.model('visitors', visitorSchema);