const express = require('express');
const app = express();
const server = require('http').createServer(app);

const db = require('./db/index');

app.use(express.json({ limit: '50mb' })); // maximum request body size 50mb
app.use(express.urlencoded({ limit: '50mb', extended: true, parameterLimit: 10000 })) // maximum request body size 50mb can handle 10000 parameters in single query

app.set('port', process.env.PORT || 3000) // to change the default port set the port in environment variable 

// api endpont
app.use('/api/visitors', require('./routes/visitors.route'));

// to catch any unhandled errors
app.use((err, req, res, next) => {
    console.error("error message " + err.message)
    console.error("error " + err.stack)
    res.status(500).send({ "statusCode": "500", "status": "error", "message": "Something went wrong" })
})

db.connectDb().then(() => {
    server.listen(app.get('port'));
    console.log(`App is running on port number ${app.get('port')}`);
});

module.exports = app;