# README #
A nodejs app with express and mongodb

### Description ###
* Server implementation for a simple museum nodejs project with mongodb as database
* Version - "1.0.0"

### Requirement ###
* For development, you will neet to install
* Node.js 10+
* Npm 6
* Mongodb 3.6+
* Git

### Steps to run ###
* git clone https://swapnilchaple@bitbucket.org/swapnilchaple/museum.git
* cd museum
* npm install
* npm start

### Database set up ###
* mongo
* use museumdb
* run insert query from /db/delta/db_qury.js

### Test ###
* npm test

### API Sample ###
* localhost:3000/api/visitors?date=1404198000000&ignore=Avila Adobe
* localhost:3000/api/visitors?date=1404198000000
