'use strict';  

// packages
const express = require('express');
const router = express.Router();

// services
const museumService = require('../services/museum.service');

// get visitors stats
router.get('/', (req, res) => {
    console.log('/api/visitors', req.query);
    let date = req.query.date;
    let ignore = req.query.ignore;
    museumService.getVisitorsStats(date, ignore).then(data => {
        res.status(200).send({
            "statusCode": "200",
            "status": "success",
            "message": "museum visitors details fetched successfully",
            "payload": data
        })
    }).catch(error => {
        res.status(400).send({
            "statusCode": "400",
            "status": "failed",
            "message": "failed to fetch data"
        })
    })
})

module.exports = router